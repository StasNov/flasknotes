import sqlite3

from flask import Flask, g, render_template, request, redirect, url_for

DATABASE_URL = './db.sqlite'
use_filter = False
query = ''
dict_for_filter = dict()
app = Flask(__name__)


def open_db():
    if not hasattr(g, 'db'):
        g.db = sqlite3.connect(DATABASE_URL)
        g.db.row_factory = sqlite3.Row
    return g.db


@app.teardown_appcontext
def close_db(exception):
    if hasattr(g, 'db'):
        g.db.close()


@app.route('/')
def index():
    global use_filter, query, dict_for_filter
    use_filter = False
    query = ''
    dict_for_filter = dict()

    db = open_db()
    cursor = db.cursor()
    notes = cursor.execute('SELECT id, name, likes, importance FROM notes').fetchall()
    return render_template('index.html', notes=notes)


@app.route('/add-form', methods=['GET'])
def add_form():
    return render_template('add.html')


@app.route('/add', methods=['POST'])
def add():
    db = open_db()
    cursor = db.cursor()
    cursor.execute(
        'INSERT INTO notes(name, content, importance) VALUES (:name, :content, :importance)',
        {
            'name': request.form['name'].strip(),
            'content': request.form['content'].strip(),
            'importance': request.form['importance']
        }
    )
    db.commit()
    return redirect(url_for('index'))


@app.route('/edit/<int:id>')
def edit(id):
    db = open_db()
    cursor = db.cursor()
    note = cursor.execute(
        'SELECT id, name, content, importance FROM notes WHERE id = :id',
        {
            'id': id
        }
    ).fetchone()
    return render_template('update.html', note=note)


@app.route('/update', methods=['POST'])
def update():
    db = open_db()
    cursor = db.cursor()
    cursor.execute(
        'UPDATE notes SET name = :name, content = :content, importance = :importance WHERE id = :id',
        {
            'name': request.form['name'].strip(),
            'content': request.form['content'].strip(),
            'importance': request.form['importance'],
            'id': request.form['id']
        }
    )
    db.commit()
    return redirect(url_for('index'))


@app.route('/remove', methods=['POST'])
def delete():
    db = open_db()
    cursor = db.cursor()
    cursor.execute(
        'DELETE FROM notes WHERE id = :id',
        {
            'id': request.form['id']
        }
    )
    db.commit()
    return redirect(url_for('index'))


@app.route('/search', methods=['GET'])
def search():
    global use_filter, query, dict_for_filter
    db = open_db()
    cursor = db.cursor()
    if 'search' in request.values and request.values['search'].strip():
        use_filter = True
        query = 'SELECT id, name, likes, importance FROM notes WHERE lower(name) LIKE lower(:search)'
        dict_for_filter = {'search': '%' + request.values['search'].strip() + '%'}
        notes = cursor.execute(
            query,
            dict_for_filter
        ).fetchall()
        return render_template('index.html', notes=notes, search=request.values['search'])
    return redirect(url_for('index'))


@app.route('/sorted/asc')
def sorted_asc():
    global use_filter, query, dict_for_filter
    db = open_db()
    cursor = db.cursor()
    if use_filter:
        notes = cursor.execute(
            query + ' ORDER BY importance ASC',
            dict_for_filter
        ).fetchall()
    else:
        notes = cursor.execute('SELECT id, name, likes, importance FROM notes ORDER BY importance ASC').fetchall()

    if dict_for_filter:
        return render_template('index.html', notes=notes, search=dict_for_filter['search'][1:-1])
    return render_template('index.html', notes=notes)


@app.route('/sorted/desc')
def sorted_desc():
    global use_filter, query, dict_for_filter
    db = open_db()
    cursor = db.cursor()
    if use_filter:
        notes = cursor.execute(
            query + ' ORDER BY importance DESC',
            dict_for_filter
        ).fetchall()
    else:
        notes = cursor.execute('SELECT id, name, likes, importance FROM notes ORDER BY importance DESC ').fetchall()

    if dict_for_filter:
        return render_template('index.html', notes=notes, search=dict_for_filter['search'][1:-1])
    return render_template('index.html', notes=notes)


@app.route('/filter_without_sort')
def without():
    global query, dict_for_filter
    db = open_db()
    cursor = db.cursor()
    if dict_for_filter and query:
        notes = cursor.execute(
            query + ' ORDER BY id',
            dict_for_filter
        ).fetchall()

        return render_template('index.html', notes=notes, search=dict_for_filter['search'][1:-1])


@app.route('/like_plus', methods=['POST'])
def like_plus():
    db = open_db()
    cursor = db.cursor()
    cursor.execute(
        'UPDATE notes SET likes = likes + 1 WHERE id = :id',
        {
            'id': request.form['id']
        }
    )
    db.commit()
    return redirect(url_for('index'))


@app.route('/like_minus', methods=['POST'])
def like_minus():
    db = open_db()
    cursor = db.cursor()
    cursor.execute(
        'UPDATE notes SET likes = likes - 1 WHERE id = :id',
        {
            'id': request.form['id']
        }
    )
    db.commit()
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run()